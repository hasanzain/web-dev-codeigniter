#
# TABLE STRUCTURE FOR: authors
#

DROP TABLE IF EXISTS `authors`;

CREATE TABLE `authors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `birthdate` date NOT NULL,
  `added` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (1, 'Jaren', 'Smitham', 'lbalistreri@example.net', '1993-11-06', '1970-11-21 18:40:07');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (2, 'Elenor', 'Padberg', 'twillms@example.org', '1977-08-29', '2010-12-09 05:36:29');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (3, 'Rebecca', 'Hauck', 'zemlak.gino@example.com', '1993-01-09', '2012-01-24 09:42:02');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (4, 'Abdul', 'Klein', 'monahan.marge@example.com', '1980-09-04', '2005-06-19 18:22:44');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (5, 'Kenna', 'Greenholt', 'ernser.gloria@example.com', '1997-04-15', '2003-03-01 13:05:10');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (6, 'Jeromy', 'Mitchell', 'gordon44@example.org', '2009-12-09', '2009-02-23 11:56:17');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (7, 'Alyson', 'Weimann', 'brenner@example.net', '2005-08-22', '1992-01-21 12:59:31');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (8, 'Brenna', 'Batz', 'collier.gina@example.com', '1985-01-20', '1983-10-05 02:49:07');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (9, 'Nathen', 'Tremblay', 'roob.rosemarie@example.org', '1982-02-04', '1986-10-04 23:19:01');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (10, 'Maverick', 'McClure', 'nstark@example.net', '1984-10-26', '1989-08-16 10:34:11');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (11, 'Gennaro', 'Wehner', 'ronaldo10@example.net', '1996-01-05', '2018-09-24 08:52:14');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (12, 'Tessie', 'Daugherty', 'heller.broderick@example.org', '1999-07-13', '2010-01-13 06:16:11');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (13, 'Mohammad', 'Gutkowski', 'cora.hahn@example.net', '2009-01-04', '1973-08-24 23:27:19');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (14, 'Glenda', 'Mayer', 'gus.jacobs@example.net', '2013-01-12', '2014-05-24 21:07:37');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (15, 'Xander', 'Skiles', 'mercedes28@example.com', '1995-11-22', '1978-07-27 23:24:03');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (16, 'Edythe', 'Howell', 'georgette97@example.org', '1988-03-17', '2001-03-29 09:25:48');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (17, 'Blanche', 'Adams', 'sthiel@example.com', '2014-11-21', '1975-08-23 11:19:57');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (18, 'Travon', 'Zulauf', 'jokuneva@example.net', '1980-10-11', '1983-10-10 12:25:49');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (19, 'Lavern', 'Hickle', 'opadberg@example.com', '1973-07-01', '2006-12-31 01:04:36');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (20, 'Rusty', 'Stehr', 'jammie89@example.com', '1981-03-29', '2015-11-25 00:09:31');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (21, 'Nathan', 'Lakin', 'emie.corkery@example.net', '1986-08-24', '1972-12-08 03:59:35');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (22, 'Odessa', 'Tremblay', 'merritt.schmitt@example.net', '1975-09-21', '1998-10-08 21:14:58');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (23, 'Henri', 'Hilpert', 'aron.lang@example.net', '2016-06-14', '2016-05-23 19:53:40');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (24, 'Ilene', 'Funk', 'simeon20@example.com', '1976-09-04', '1996-08-03 09:46:45');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (25, 'Jalon', 'Boyle', 'kd\'amore@example.net', '2008-12-21', '2012-10-22 06:58:38');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (26, 'Eileen', 'Ratke', 'vgoldner@example.org', '2017-07-08', '1990-08-09 16:41:46');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (27, 'Mustafa', 'Botsford', 'dejah89@example.com', '2013-12-27', '1998-05-10 18:20:07');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (28, 'Christina', 'Spinka', 'jones.esperanza@example.net', '1994-09-05', '2006-07-01 06:18:34');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (29, 'Alysha', 'Lueilwitz', 'roob.wilton@example.com', '2017-11-26', '1982-03-11 21:08:03');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (30, 'Kelsi', 'Weber', 'daisy35@example.org', '1971-09-29', '2001-04-24 17:08:13');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (31, 'Emerson', 'Effertz', 'dwiegand@example.com', '1980-05-01', '1991-02-11 16:48:16');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (32, 'Tressa', 'Veum', 'marlin.kshlerin@example.net', '1994-04-21', '1993-12-24 21:32:12');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (33, 'Eliezer', 'Ritchie', 'isabell10@example.net', '2009-01-17', '1994-09-29 01:32:35');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (34, 'Rhoda', 'Powlowski', 'emacejkovic@example.net', '2002-09-10', '2018-01-09 00:55:00');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (35, 'Mattie', 'Spencer', 'mafalda.fahey@example.com', '1972-06-13', '1997-05-11 16:03:22');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (36, 'Henderson', 'Olson', 'vhaag@example.com', '2010-02-03', '2000-05-08 00:06:54');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (37, 'Karl', 'Vandervort', 'dubuque.ollie@example.com', '2010-02-19', '2011-05-25 15:01:07');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (38, 'Demario', 'Johnston', 'cristal49@example.net', '1971-11-16', '1980-09-12 03:33:37');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (39, 'Clemmie', 'Kuvalis', 'glen.towne@example.net', '1979-06-08', '1983-09-11 21:35:57');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (40, 'Cordelia', 'Mayert', 'von.earl@example.net', '1986-04-07', '2012-09-04 04:02:31');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (41, 'Greta', 'Reinger', 'philip82@example.org', '1982-11-07', '2001-01-26 23:55:31');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (42, 'Belle', 'Welch', 'vincenzo.yost@example.com', '2015-02-16', '2007-08-11 10:18:28');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (43, 'Kylee', 'Muller', 'tkulas@example.org', '1997-07-29', '1973-04-23 08:29:51');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (44, 'Mckayla', 'Kihn', 'allison.wilkinson@example.net', '1999-06-24', '1988-02-07 04:23:41');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (45, 'Deangelo', 'Hintz', 'genevieve16@example.com', '2015-08-20', '2008-10-11 20:21:12');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (46, 'Ottilie', 'Dicki', 'frank.gulgowski@example.com', '1999-12-28', '2019-02-22 19:43:28');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (47, 'Afton', 'Brekke', 'eloise35@example.org', '1977-12-16', '1992-07-20 23:59:34');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (48, 'Harry', 'Casper', 'hobart19@example.net', '2011-01-18', '1989-02-27 11:52:12');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (49, 'Helena', 'Robel', 'uhessel@example.com', '2001-06-10', '1983-09-26 10:58:42');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (50, 'Nels', 'Hartmann', 'eduardo50@example.net', '1977-04-17', '2000-09-03 11:05:20');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (51, 'Jane', 'Turcotte', 'ikshlerin@example.com', '1971-04-01', '1987-05-01 03:12:54');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (52, 'Julian', 'Abshire', 'idell.zemlak@example.org', '1996-07-02', '1992-06-20 00:29:17');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (53, 'Moriah', 'Graham', 'astreich@example.net', '1971-01-13', '2001-03-27 00:02:21');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (54, 'Frederik', 'Lemke', 'hilpert.josiah@example.net', '2015-11-18', '1980-01-07 14:23:03');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (55, 'Maiya', 'Mann', 'antonia.medhurst@example.org', '2006-03-28', '1985-04-16 22:18:23');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (56, 'Adolphus', 'Carroll', 'ivory89@example.net', '2017-11-20', '2006-10-10 00:27:26');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (57, 'Darion', 'Shields', 'ifisher@example.org', '1973-05-22', '1996-10-01 11:02:24');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (58, 'Deanna', 'Rau', 'hartmann.lawrence@example.net', '1976-10-13', '2013-05-05 10:24:31');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (59, 'Ozella', 'Moen', 'gudrun.oberbrunner@example.net', '1983-05-22', '1992-09-18 04:37:54');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (60, 'Emiliano', 'Schultz', 'kellen07@example.net', '1983-01-03', '2018-06-20 19:24:28');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (61, 'Tina', 'Roob', 'o\'reilly.winnifred@example.net', '1981-05-14', '2007-08-19 10:36:56');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (62, 'Ephraim', 'Lowe', 'isai.mccullough@example.net', '2016-07-15', '1978-09-02 14:26:56');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (63, 'Roma', 'Parker', 'esther79@example.com', '2006-01-11', '1994-08-25 16:22:05');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (64, 'Zelda', 'Fadel', 'zwyman@example.org', '2012-08-29', '2016-05-17 20:37:52');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (65, 'Jaeden', 'Becker', 'werner50@example.net', '2018-03-08', '1975-06-30 11:14:46');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (66, 'Clare', 'Wyman', 'jdonnelly@example.org', '2010-11-08', '1974-02-19 20:40:36');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (67, 'Rollin', 'Miller', 'sylvan41@example.com', '1988-09-28', '2010-02-25 22:42:00');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (68, 'Luciano', 'Thiel', 'greyson.koelpin@example.org', '2003-07-01', '2018-07-27 15:14:41');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (69, 'Alberta', 'Kuhn', 'jett.hirthe@example.net', '1975-02-10', '1983-10-22 07:57:27');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (70, 'Christa', 'Krajcik', 'blick.madaline@example.org', '1982-02-20', '2009-05-20 06:43:44');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (71, 'Gilberto', 'Block', 'kuphal.ludie@example.net', '1996-10-04', '2011-06-07 12:56:08');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (72, 'Gerda', 'Towne', 'jaida01@example.org', '1994-12-11', '2003-01-07 19:43:38');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (73, 'Laila', 'Kunde', 'nraynor@example.net', '1977-11-01', '1980-03-09 04:10:43');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (74, 'Charlene', 'Hoppe', 'walker65@example.org', '1978-03-08', '1974-10-23 14:39:41');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (75, 'Madilyn', 'Boyer', 'price.flossie@example.org', '1972-04-19', '1971-07-25 10:37:30');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (76, 'Adriana', 'Skiles', 'edgar72@example.com', '2000-12-26', '1989-11-20 21:11:34');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (77, 'Bartholome', 'Kuvalis', 'frami.glenna@example.org', '1982-10-29', '1989-04-26 19:19:58');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (78, 'Allan', 'Watsica', 'abernathy.sedrick@example.com', '2009-04-03', '2015-08-24 05:19:21');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (79, 'Braxton', 'Effertz', 'aurelia91@example.org', '1985-03-09', '1997-04-16 13:07:42');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (80, 'Heaven', 'Bashirian', 'ricardo.cummerata@example.net', '2004-06-14', '2001-04-14 23:30:34');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (81, 'Eino', 'Spencer', 'mohr.d\'angelo@example.com', '2005-08-02', '1991-05-22 15:01:23');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (82, 'Dolly', 'Wisozk', 'grady.albina@example.com', '1970-10-10', '2004-10-13 21:13:01');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (83, 'Daisy', 'Kuhn', 'rluettgen@example.com', '1975-01-14', '1997-08-14 21:35:33');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (84, 'Lillian', 'Mayert', 'will.brenda@example.org', '1972-02-10', '2005-07-13 13:14:18');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (85, 'Evans', 'Pollich', 'johnston.maxie@example.org', '1991-02-15', '1987-03-03 15:29:36');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (86, 'Rickey', 'Sanford', 'yasmeen46@example.net', '1993-12-30', '1976-02-29 17:47:40');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (87, 'Mozelle', 'Collins', 'shansen@example.org', '1989-02-05', '2001-09-15 22:30:27');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (88, 'Gaston', 'Kuphal', 'xwisoky@example.org', '2015-05-08', '2014-11-10 20:53:38');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (89, 'Abraham', 'Bauch', 'giovanny61@example.net', '1992-10-13', '2015-09-14 16:50:48');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (90, 'Ivory', 'Mraz', 'sarina.schuster@example.net', '1991-09-19', '1995-09-04 21:01:09');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (91, 'Josiane', 'Braun', 'antonio.grimes@example.net', '1994-11-23', '1976-04-12 04:19:48');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (92, 'Shanny', 'Leannon', 'diego.o\'conner@example.net', '1994-08-04', '1971-02-26 16:53:02');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (93, 'Aliya', 'Murray', 'boyle.lonzo@example.com', '1972-11-17', '2010-09-02 20:17:21');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (94, 'Obie', 'Bartell', 'beatty.madge@example.net', '2016-09-04', '1976-08-23 03:46:56');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (95, 'Litzy', 'McLaughlin', 'shana05@example.org', '2015-07-10', '1998-09-13 09:27:43');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (96, 'Jayne', 'Harvey', 'prohaska.ardith@example.net', '1993-07-13', '1972-04-22 08:30:32');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (97, 'Urban', 'Waelchi', 'santina73@example.net', '1986-02-13', '2016-01-28 05:31:47');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (98, 'Michael', 'Collier', 'delphine.bernier@example.net', '1991-12-22', '2014-11-26 22:32:29');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (99, 'Tess', 'Hoeger', 'marie.hyatt@example.org', '1970-08-04', '1974-08-08 01:55:14');
INSERT INTO `authors` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `added`) VALUES (100, 'Ahmed', 'Ortiz', 'mwillms@example.net', '1970-12-24', '1993-02-16 18:13:20');


#
# TABLE STRUCTURE FOR: posts
#

DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

